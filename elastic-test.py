from time import sleep
import csv
from elasticsearch import Elasticsearch

INDEX = 'fast_food_restaurants'

def connect_elasticsearch(print_elastic_info:bool = False) -> Elasticsearch:
	_es = None
	_es = Elasticsearch("localhost:9200")
	if _es.ping():
		if print_elastic_info: print(_es.info())
		print('Yay Connected')
	else:
		print('Awww it could not connect!')
	return _es


def parse_csv() -> list:
	data = []
	with open('data/FastFoodRestaurants.csv', 'r') as csv_file:
		read = csv.reader(csv_file)
		for row in read:
			data.append(row)
		csv_file.close()

	del data[0] # drop header
	return data

if __name__ == '__main__':
	es = connect_elasticsearch()

	data = parse_csv()

	if len(data) > 0:
		for index, row in enumerate(data):
			parsed_data = {
				"index": row[0],
				'address': row[1],
				'city': row[2],
				'country': row[3],
				'keys': row[4],
				'latitude': row[5],
				'longitude': row[6],
				'name': row[7],
				'postalCode': row[8],
				'province': row[9],
				'websites': row[10],
			}

			response_create = es.index(index=INDEX, doc_type="restaurants", id=index+1, body=parsed_data)
			print(f"Data /w index {index} {response_create['result']} | document version: {response_create['_version']}")

			sleep(0.01)

	else:
		print('Something went wrong')

