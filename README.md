# Setup

## Docker

```sh
sudo docker-compose -f service.yml up -d
```


## Elastic

localhost:9200


## Kibana

localhost:5600


### Kibana Dev tools

* Get cluster health - `GET _cluster/health`
* List all indices - `GET /_cat/indices`
* Query items
	```shell
		GET fast_food_restaurants/_search
	{
		"query": {
			"match_all": {}
		}
	}
	```